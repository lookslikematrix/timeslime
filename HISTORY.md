# 2.3

* Extend `report` with end of working time, when it's today.

# 2.2

* Add `version` command for getting `timeslime` version

* bugfix:

    * 🐛 Add missing `click` context to `stop` command

    * 🐛 Make it possible to execute `report` without configuration

# 2.1

* Make database file path configurable with `database-file-path`

# 2.0

* Refactor `timeslime`

# 1.7
git 
* improve `display` command with `rich`

# 1.6

* start and stop time can now be set by parameter

* drop support for python 3.7 and 3.8

# 1.5

* use UTC time

* bugfix:

    * get correct elapsed time at first start up of the current day

    * use ISO 8601 dateformat to handle dates correct

# 1.4

* add `sync` command to CLI to force synchronization

* add questions for timeslime_server, username and password to init

* add timeslime-server authentication

* add compatibility to [*timeslime-server*](https://gitlab.com/lookslikematrix/timeslime-server) >= v1.2.2

* bugfix: 

    * deserialize also on different date format

    * set updated_at property only when there is a change

    * send updated model to server

# 1.3

* sync settings/timespans with server

* add compatibility to [*timeslime-server*](https://gitlab.com/lookslikematrix/timeslime-server) >= v1.1.1

* use [peewee](http://docs.peewee-orm.com) to access database

* extend setting/timespan with created_at and updated_at column

# 1.2

* move raspberry pi stuff to own repository [https://gitlab.com/lookslikematrix/timeslime-rpi/](https://gitlab.com/lookslikematrix/timeslime-rpi/)

* send data to [*timeslime-server*](https://gitlab.com/lookslikematrix/timeslime-server)

* send settings to [*timeslime-server*](https://gitlab.com/lookslikematrix/timeslime-server)

* get settings from [*timeslime-server*](https://gitlab.com/lookslikematrix/timeslime-server)

* make most things configurable (working hours, timeslime-server address, database path)

* make sure a NTP server is connected to the system to get accurate time

# 1.1

* add Raspberry Pi script to start/stop with a button and display elapsed time with a display

* publish `timeslime` on PyPI

* add console script

* bugfix: previous timespan is not changed anymore

# 1.0

* add script to start/stop/display time