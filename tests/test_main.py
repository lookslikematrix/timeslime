"""main CLI test file"""

from os import remove
from re import match
from click.testing import CliRunner
from assertpy import assert_that

from peewee import SqliteDatabase

from timeslime.__main__ import config, main, version

RUNNER = CliRunner()

class TestMain:
    """main CLI test"""
    def test_help_success(self):
        """test timeslime command"""
        # arrange & act
        response = RUNNER.invoke(main)

        # assert
        assert_that(response.exit_code).is_equal_to(0)
        assert_that(response.output).is_equal_to(
            "Usage: main [OPTIONS] COMMAND [ARGS]...\n\n"
            "  ⌚ This is timeslime! Have fun tracking your time ⌚\n\n"
            "  Read here for further information:\n\n"
            "      https://gitlab.com/lookslikematrix/timeslime\n\n"
            "  If you've any issues report them here:\n\n"
            "      https://gitlab.com/lookslikematrix/timeslime/-/issues/new\n\n"
            "Options:\n"
            "  --loglevel TEXT            Set loglevel (default: WARNING)\n"
            "  --database-file-path TEXT  Set database file path\n"
            "  --help                     Show this message and exit.\n\n"
            "Commands:\n"
            "  config   🔧 Get or set timeslime configuration 🔧\n"
            "  report   📊 report your time 📊\n"
            "  start    🚀 start your time 🚀\n"
            "  stop     🛑 stop your time 🛑\n"
            "  sync     🤝 synchronize your time of today 🤝\n"
            "  version  📦 get version number from timeslime 📦\n"

        )

    def test_config_help(self):
        """test config command"""
        # arrange & act
        response = RUNNER.invoke(
            config,
            args= "--help"
        )

        # assert
        assert_that(response.exit_code).is_equal_to(0)
        assert_that(response.output).is_equal_to(
            "Usage: config [OPTIONS]\n\n"
            "  🔧 Get or set timeslime configuration 🔧\n\n"
            "Options:\n"
            "  --weekly-working-hours INTEGER  Get/set your weekly working hours\n"
            "  --timeslime-server TEXT         Get/set your timeslime server URL\n"
            "  --username TEXT                 Get/set your timeslime username\n"
            "  --password TEXT                 Get/set your timeslime password\n"
            "  --help                          Show this message and exit.\n"
        )

    def test_config_weekly_working_hours(self):
        """test config command"""
        # arrange & act
        response = RUNNER.invoke(
            config,
            args= "--weekly-working-hours=38",
            obj={
                "DATABASE": SqliteDatabase("test.db")
            }
        )

        # assert
        assert_that(response.exit_code).is_equal_to(0)
        assert_that(response.output).is_equal_to(
            "                       🔧 weekly working hours config 🔧                        \n"
            "┏━━━━━━━━━━┳━━━━━━━━━━┳━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━┓\n"
            "┃ 😩       ┃ 🌟       ┃ 🐪       ┃ 💪        ┃ 🎉       ┃ 🌞        ┃ 😌       ┃\n"
            "┃ Monday   ┃ Tuesday  ┃ Wednesd… ┃ Thursday  ┃ Friday   ┃ Saturday  ┃ Sunday   ┃\n"
            "┡━━━━━━━━━━╇━━━━━━━━━━╇━━━━━━━━━━╇━━━━━━━━━━━╇━━━━━━━━━━╇━━━━━━━━━━━╇━━━━━━━━━━┩\n"
            "│ 7:36:00  │ 7:36:00  │ 7:36:00  │ 7:36:00   │ 7:36:00  │ 0:00:00   │ 0:00:00  │\n"
            "└──────────┴──────────┴──────────┴───────────┴──────────┴───────────┴──────────┘\n"
        )
        remove("test.db")

    def test_version_happy_path(self):
        """test config command"""
        # arrange & act
        response = RUNNER.invoke(
            version
        )

        # assert
        assert_that(response.exit_code).is_equal_to(0)

        match_response = match(r"(?P<version>(\d+\.){2,3}\d+)", response.output)
        version_match = match_response.group("version")
        assert_that(version_match).is_not_empty()
